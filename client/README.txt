RobotRage Rearmed Client 1.0.4
By Anthony Kleine

	This client adds a variety of patches and features to
	the game RobotRage Retro, including UPnP support. Both
	Desktop and Web compatible (via Shockwave Player.)

To Build From Source:
	-Install Macromedia Director MX 2004 and update it to the latest version (10.1.1.16.)
	-Replace Projec32.skl in Director's install folder with the one included with the project source.
	-Install the LeechProtectionRemovalHelp Xtra and UPnP Xtra to Director.
	-Open the wrapper_robotragerearmed Director Movie in Director.
	-Go to File > Publish.
	-This will create a Projector and Shockwave Movie for desktop and web use respectively.
	-Open the browser Director Movie in Director.
	-Go to File > Publish. This will create a browser Shockwave Movie.
	-Open the login013 Director Movie in Director.
	-Go to File > Publish. This will create a login013 Shockwave Movie.

To Run The Binary:
	-copy the Projectors (robotragerearmed) and Shockwave Movies (wrapper_robotragerearmed, browser, and login013) into the v55/v60 folder provided, depending on the version of the game you wish to run.