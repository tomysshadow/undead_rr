<?php
/**********************************************
* Downloads bot from database and checks if   *
* player's eq is possible with his number of  *
* wins.                                       *
***********************************************/
function anticheatSumCredits($data, $sql, $config)
{
	$query = mysqli_query($sql, "SELECT * FROM bots WHERE id=".$data['robotid']);
	$bot = mysqli_fetch_array($query);
	
	$sum=-435; //stating bot price
	
	$sum+=getPartPrice($bot['ram']);
	$sum+=getPartPrice($bot['torn']);
	$sum+=getPartPrice($bot['hjul']);
	$sum+=getPartPrice($bot['motor']);
	
	$sum-=$bot['anticheat_diff'];

	if(!empty($bot['batteri']))
	{
		$i = explode(',', $bot['batteri']);
		foreach($i as $b)
			$sum+=getPartPrice($b);
	}
	
	if(!empty($bot['vapen']))
	{
		$i = explode(',', $bot['vapen']);
		foreach($i as $b)
			$sum+=getPartPrice($b);
	}
	
	if(!empty($bot['special']))
	{	
		$i = explode(',', $bot['special']);
		foreach($i as $b)
			$sum+=getPartPrice($b);
	}

	if(!empty($bot['inventory']))
	{
		$i = explode(',', $bot['inventory']);
		foreach($i as $b)
			$sum+=getPartPrice($b);
	}
		
	$sum+=$bot['money'];
	
	if($config['debug_mode'])
		file_put_contents('logs/anticheat.txt', '***'.PHP_EOL. $bot['RobotName'].PHP_EOL .'sum: '.$sum.PHP_EOL .'max: '.($bot['win'] * 10).PHP_EOL , FILE_APPEND | LOCK_EX);
	
	if(($bot['win'] * 10) < $sum)
	{
		//oy, we got a sneaky cheater over here mate!
		ban($data, $sql, $config, 'duplicating items');
	}
	else if(($bot['win'] * 10) > $sum)
	{
		//he sold something, and that changed max amount of money he can have (-10% on price)
		$new_diff = $sum - ($bot['win'] * 10); 
		mysqli_query($sql, 'UPDATE bots SET anticheat_diff='.$new_diff.' WHERE id="'.$bot['id'].'"');
	}
}

/**********************************************
* Downloads bot from database and checks if   *
* player's parts placement is possible        *
***********************************************/
function anticheatVerifyBotIntegrity($data, $sql, $config)
{
	$query = mysqli_query($sql, "SELECT * FROM bots WHERE id=".$data['robotid']);
	$bot = mysqli_fetch_array($query);
	
	$frontWpn=0;
	$backWpn=0;
	$towerWpn=0;
	
	if(!empty($bot['wpnPos']))
	{
		$i = explode(',', $bot['wpnPos']);
		foreach($i as $b)
		{
			switch(trim($b))
			{
				case "#f":
					$frontWpn++;
					break;
				case "#b":
					$backWpn++;
					break;	
				case "#t":
					$towerWpn++;
					break;
			}		
		}
		
		if($frontWpn > 1 or $backWpn > 1 or $towerWpn > 1)
		{
			//cheater again mates!
			ban($data, $sql, $config, 'impossible bot setup');
		}
	}
	
	if(($towerWpn >= 1 and $bot['torn']=="") or $bot['pansar'] > 255)
	{
		ban($data, $sql, $config, 'impossible bot setup');
	}
}



/**********************************************
* Downloads bot from database and checks if   *
* update is realistic                         *
***********************************************/
function anticheatVerifyUpdate($data, $sql, $config)
{
	/*$query = mysqli_query($sql, "SELECT * FROM bots WHERE id=".$data['robotid']);
	$bot = mysqli_fetch_array($query);*/
	//TODO - just a stub now
}



function ban($data, $sql, $config, $reason)
{
	$query = mysqli_query($sql, "SELECT * FROM bots WHERE id=".$data['robotid']);
	$bot = mysqli_fetch_array($query);
	
	mysqli_query($sql, 'UPDATE accounts SET ban=1, ban_reason="'.$reason.'", banned_by="Automatic Cheats Detector" WHERE login="'.$bot['owner'].'"');
	
	if(!$config['dont_block_banned_bots'])
	{
		echo "[#Status: 0]";
		die();
	}
}

function getPartPrice($part)
{
	switch(trim($part))
	{
		case "#r1":
			return 150;
		case "#r2":
			return 200;
		case "#r3":
			return 400;
		case "#t1":
			return 100;
		case "#t2":
			return 130;
		case "#t3":
			return 250;
		case "#h1":
			return 50;
		case "#h2":
			return 75;
		case "#h3":
			return 125;
		case "#h4":
			return 100;
		case "#h5":
			return 150;
		case "#ma1":
			return 100;
		case "#ma2":
			return 150;
		case "#ma3":
			return 300;
		case "#mb1":
			return 400;
		case "#mb2":
			return 500;
		case "#mb3":
			return 700;
		case "#b1":
			return 50;
		case "#b2":
			return 100;
		case "#v13":
			return 75;
		case "#v2":
			return 350;
		case "#v15":
			return 71;
		case "#v6":
			return 25;
		case "#v7":
			return 10;
		case "#v8":
			return 50;
		case "#v14":
			return 80;
		case "#v10":
			return 70;
		case "#v11":
			return 70;
		case "#v12":
			return 110;
		case "#v21":
			return 500;
		case "#v22":
			return 750;
		case "#v23":
			return 100;
		case "#v24":
			return 150;
		case "#v25":
			return 150;
		case "#v26":
			return 200;	
		
		//superpack1
		case "#v1":
		case "#v3":
		case "#v4":
		case "#v9":
		case "#v17":
		case "#v16":
		case "#b3":
		//superpack2
		case "#v31":
		case "#v32":
		case "#v33":
		case "#v34":
		case "#v35":
		case "#v36":
		case "#v37":
		
			return 300;	
		default:
			return 0;
	}
}
?>