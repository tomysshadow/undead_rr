<?php
	//server setup
	$config['sql_host']='127.0.0.1';			//SQL server address; probably 127.0.0.1
	$config['sql_user']='';						//SQL username; don't use root, I do not guarantee the server is bulletproof
	$config['sql_pass']='';						//SQL password
	$config['sql_db']='robotrage';				//name of SQL database
	
	//advanced settings
	$config['number_of_hiscores_displayed']=50;	//default: 50
	$config['passwords_salt']='VggzySbmGP9eFH'; //Random string added to passwords when hashing. Change it before setting up the server. Warning: if you change it, all accounts created before will stop working!
	$config['minimum_password_length']=6; 		//default: 6
	$config['anticheat_enabled']=true;			//default: true
	$config['disable_account_creator']=false;	//default: false; Will show error when trying to register new account if set to true

	//debug options, leave tham as they are
	$config['debug_mode']=true;					//default: false; if true, will accept commands through GET and drop logs into logs/ directory. Warning: unhashed passwords are stored in the log!
	$config['allow_banned_login']=true;			//default: false; this will work as ban on single bot instead of account
	$config['dont_block_banned_bots']=true;		//default: false; this will allow banned bots to enter the game
?>