<?php

require_once("anticheat.php");

function getHiscores($sql, $config)
{
	$full='[#hiscore:[';
	$all=mysqli_query($sql, "SELECT RobotName, Score FROM bots ORDER BY Score DESC LIMIT ".$config['number_of_hiscores_displayed']);
	$cnt=1;
	if($all) {
		while($row = mysqli_fetch_assoc($all))
		{
			if($cnt!=1)
				$full.=',';
			
			$full.='[#Count:'.$cnt.',#RobotName:"'.$row['RobotName'].'", #Score:'.$row['Score'].']';
			$cnt++;
		}
		
		$full.="]]";
	}	
	echo $full;
}

function deleteRobot($data, $sql, $config)
{
	$bot = mysqli_fetch_array(mysqli_query($sql, "SELECT count FROM bots WHERE id=".$data['deleterobot']));
	$row = "bot".$bot['count'];
	mysqli_query($sql, 'DELETE FROM bots WHERE id="'.$data['robotid'].'"');
	mysqli_query($sql, 'UPDATE accounts SET '.$row.'="" WHERE login="'.getLoginFromSid($data['sessionid'], $sql).'"');
	echo "[#status:0]";
}

function getRobotList($data, $sql, $config)
{
	//anticheatSumCredits($data, $sql, $config);
	
	$full = "[#err:0,#robotlist:[";
	$cnt=1;
	
	$all = mysqli_query($sql, "SELECT id, RobotName FROM bots WHERE owner='".getLoginFromSid($data['sessionid'], $sql)."'");
	
	if($all) {
		while($row = mysqli_fetch_assoc($all))
		{
			if($cnt!=1)
				$full.=',';
			
			$full.='[#count:'.$cnt.',#robotname:"'.$row['RobotName'].'",#robotid:'.$row['id'].',#password:"nop"]';
			$cnt++;
		}
		
		$full.="]]";
	}	
	
	echo $full;
}

function createRobot($data, $sql, $config)
{
	if(empty($data['sessionid']))
		return;

	$cnt = mysqli_fetch_array(mysqli_query($sql, "SELECT COUNT(*) AS bots FROM bots WHERE owner='".getLoginFromSid($data['sessionid'], $sql)."'"));

	if($cnt['bots']>4)
	{
		echo '[#err:"because you have already 5 bots."]';
		return;
	}
	
	$cnt2 = mysqli_fetch_array(mysqli_query($sql, "SELECT COUNT(*) AS bots FROM bots WHERE RobotName='".$data['robotname']."'"));

	if($cnt2['bots']>0)
	{
		echo '[#err:"because there is already bot with that name."]';
		return;
	}

	$newid=rand(10000000,99999999);
	
	mysqli_query($sql, "INSERT INTO bots (id, owner, ram, torn, hjul, motor, batteri, vapen, wpnPos, special, ammunition, inventory, money, space, spaceleft, maxSpeed, pansar, info1, saveMap, soldweapons, Friendslist, RobotName, Score, UserLibName, win, lose, draw, map) VALUES ('".$newid."', '".getLoginFromSid($data['sessionid'], $sql)."', '#r1', '', '#h2', '#ma1', '#b1', '', '', '', '', '', '50', '100', '10', '40', '55', '1062', '0', '', '', '".$data['robotname']."', '0', '".$newid."', '0', '0', '0', '')");
	
	echo '[#status:0]';
}


function saveMap($data, $sql, $config)
{
	mysqli_query($sql, "UPDATE bots SET map='".$data['map']."' WHERE id=".$data['robotid']);
}

function getMap($data, $sql, $config)
{
	$query = mysqli_query($sql, "SELECT map FROM bots WHERE id=".$data['robotid']);
	$bot = mysqli_fetch_array($query);
	echo $bot['map'];
	
}

function resetBattleCounter($data, $sql, $config)
{
	$serverid = findValue("server", $data['gamegescription']);
	$clientid = findValue("client", $data['gamegescription']);
	
	
	//$logfile = file_put_contents('log.txt', '*****************************************'.PHP_EOL .'$serverid: '.$serverid.PHP_EOL , FILE_APPEND | LOCK_EX);
	
	mysqli_query($sql, 'UPDATE bots SET anticheat_battle_cnt = 0 WHERE id='.$serverid.' OR id='.$clientid);
	
	echo '[#status:0]';
}

function updateScore($data, $sql, $config)
{
	if($data['winner']==$data['user1id'])
		$data['loser']=$data['user2id'];
	else
		$data['loser']=$data['user1id'];
	
	//draw
	if($data['winner']=="0")
		return;
	
	
	$loser = mysqli_fetch_array(mysqli_query($sql, 'SELECT * FROM bots WHERE id='.$data['loser']));
	$winner = mysqli_fetch_array(mysqli_query($sql, 'SELECT * FROM bots WHERE id='.$data['winner']));
	
	if($winner['anticheat_battle_cnt']>3)
	{
		//cheater
		ban($data, $sql, $config, "tampering with scores");
	}
	
	if($loser['Score']>=4000)
		$loser['rank']=5;
	else if($loser['Score']>=1999)
		$loser['rank']=4;
	else if($loser['Score']>=999)
		$loser['rank']=3;
	else if($loser['Score']>=499)
		$loser['rank']=2;
	else if($loser['Score']>=249)
		$loser['rank']=1;
	else
		$loser['rank']=0;
	
	if($winner['Score']>=4000)
		$winner['rank']=5;
	else if($winner['Score']>=1999)
		$winner['rank']=4;
	else if($winner['Score']>=999)
		$winner['rank']=3;
	else if($winner['Score']>=499)
		$winner['rank']=2;
	else if($winner['Score']>=249)
		$winner['rank']=1;
	else
		$winner['rank']=0;
	
	if($winner['rank']!=$loser['rank'])
		return;
	
	if($winner['Score']>=$loser['Score'])
	{
		//winner is stronger, no additional rewards or punishments
		mysqli_query($sql, 'UPDATE bots SET win = win+1, Score = Score+10, anticheat_battle_cnt = anticheat_battle_cnt + 1 WHERE id='.$data['winner']);
		mysqli_query($sql, 'UPDATE bots SET lose = lose+1, anticheat_battle_cnt = anticheat_battle_cnt + 1 WHERE id='.$data['loser']);
	}
	else
	{
		//loser is stronger, additional reward and punishment
		$score_diff = floor(($loser['Score'] - $winner['Score'])/4) + 10;
		
		mysqli_query($sql, 'UPDATE bots SET win = win+1, Score = Score+'.$score_diff.' WHERE id='.$data['winner']);
		mysqli_query($sql, 'UPDATE bots SET lose = lose+1, Score = Score-'.$score_diff.' WHERE id='.$data['loser']);
	}
}

function saveRobot($data, $sql, $config)
{
	//TODO: check if $data["robotid"] matches user in $data["sessionid"]
	
	anticheatSumCredits($data, $sql, $config);
	anticheatVerifyBotIntegrity($data, $sql, $config);
	
	$ram = findValue("ram", $data["proplist"]);
	$torn = findValue("torn", $data["proplist"]);
	$hjul = findValue("hjul", $data["proplist"]);
	$motor = findValue("motor", $data["proplist"]);
	$batteri = findValue("batteri", $data["proplist"]);
	$vapen = findValue("vapen", $data["proplist"]);
	$wpnPos = findValue("wpnPos", $data["proplist"]);
	$special = findValue("special", $data["proplist"]);
	$ammunition = findValue("ammunition", $data["proplist"]);
	$inventory = findValue("inventory", $data["proplist"]);
	$money = findValue("money", $data["proplist"]);
	$space = findValue("space", $data["proplist"]);
	$spaceleft = findValue("spaceleft", $data["proplist"]);
	$maxSpeed = findValue("maxSpeed", $data["proplist"]);
	$pansar = findValue("pansar", $data["proplist"]);
	$info1 = findValue("info1", $data["proplist"]);
	$saveMap = findValue("saveMap", $data["proplist"]);
	$soldweapons = findValue("soldweapons", $data["proplist"]);


	$q='UPDATE bots SET 
	ram="'.$ram.'",
	torn="'.$torn.'",
	hjul="'.$hjul.'",
	motor="'.$motor.'",
	batteri="'.$batteri.'",
	vapen="'.$vapen.'",
	wpnPos="'.$wpnPos.'",
	special="'.$special.'",
	ammunition="'.$ammunition.'",
	inventory="'.$inventory.'",
	money='.$money.',
	space='.$space.',
	spaceleft='.$spaceleft.',
	maxSpeed='.$maxSpeed.',
	pansar='.$pansar.',
	info1='.$info1.',
	saveMap='.$saveMap.',
	soldweapons="'.$soldweapons.'"
	WHERE id='.$data['robotid'];

	mysqli_query($sql, $q);
	echo "[#status:1]";
}

function getRobot($data, $sql, $config)
{	
	//run anticheat only on player's bot, not on bots in lobby.
	//we don't want to ban people just for viewing cheater's bot in lobby
	if($data["action"]=="getrobot")
	{
		anticheatSumCredits($data, $sql, $config);
		anticheatVerifyBotIntegrity($data, $sql, $config);
	}

	$query = mysqli_query($sql, "SELECT * FROM bots WHERE id=".$data['robotid']);
	$bot = mysqli_fetch_array($query);

	$propertyList='[#robotDescription: '.
    '[#ram: ['.
	$bot['ram'].
	'], #torn: ['.
	$bot['torn'].
	'], #hjul: ['.
	$bot['hjul'].
	'], #motor: ['.
	$bot['motor'].
	'], #batteri: ['.
	$bot['batteri'].
	'], #vapen: ['.
	$bot['vapen'].
	'], #wpnPos: ['.
	$bot['wpnPos'].
	'], #special: ['.
	$bot['special'].
	'], #ammunition: ['.
	$bot['ammunition'].
	']], #inventory: ['.
	$bot['inventory'].
	'], #money: '.
	$bot['money'].
	', #space: '.
	$bot['space'].
	', #spaceleft: '.
	$bot['spaceleft'].
	', #maxSpeed: '.
	$bot['maxSpeed'].
	', #pansar: '.
	$bot['pansar'].
	', #info1: '.
	$bot['info1'].
	', #saveMap: '.
	$bot['saveMap'].
	', #soldweapons: ['.
	$bot['soldweapons'].
	']]';
	
	
$friendslist = '['.$bot['Friendslist'].']';
$GameStats = '[#Win: '.$bot['win'].', #lose: '.$bot['lose'].', #draw: '.$bot['draw'].']';

$checksum=md5(strtoupper(str_replace(' ', '', $propertyList . $friendslist . $GameStats) . 'md5istheshitlkasdfkhj'));

	echo 
	'[#status: 0,'.
	' #propertylist: '.
	$propertyList.
	', #friendslist: '.
	$friendslist.
	', #robotname: "'.
	$bot['RobotName'].
	'", #score: '.
	$bot['Score'].
	', #UserLibName: "'.
	$bot['UserLibName'].
	'", #GameStats: '.
	$GameStats.
	', #checksum: "'.
	$checksum.
	'", #map: "'.
	$bot['map'].
	'"]';

}      
?>