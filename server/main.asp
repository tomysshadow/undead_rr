<?php
header('Content-Encoding: plain-text');
/*****************************************************
*todo: secure all bot operations with client verification
*                                                    *
*                                                    *
******************************************************/


require_once("config.php");
require_once("sql.php");

require_once("account.php");
require_once("robot.php");
require_once("data_operations.php");
require_once("anticheat.php");

//set main.asp?dbg&... to read from GET instead of POST


if($config['debug_mode'] and isset($_GET['dbg']))
{
	$data = $_GET;
}
else
{
	error_reporting(0);
	
	$data = array_map(function($var) use ($sql){
		return mysqli_real_escape_string($sql, $var);
	},$_POST);
	
}

if($config['debug_mode'])
	$logfile = file_put_contents('logs/server.txt', '*****************************************'.PHP_EOL .'$data: '.print_r($data, true).PHP_EOL , FILE_APPEND | LOCK_EX);

	
switch ($data["action"])
{
    case "getmyrobots":
        getRobotList($data, $sql, $config);
        break;
	case "getrobot":
        getRobot($data, $sql, $config);
        break;
	case "getrobotall":
        getRobot($data, $sql, $config);
        break;
	case "getrobotmap":
		getMap($data, $sql, $config);
		break;
	case "setrobotmap":
		saveMap($data, $sql, $config);
		break;
	case "setrobot":
		saveRobot($data, $sql, $config);
        break;	
	case "gethiscore":
		getHiscores($sql, $config);
        break;
	case "loginmember":
		login($data, $sql, $config);
		break;
    case "createmember":
        createAccount($data, $sql, $config);
        break;
	case "createrobot":
		createRobot($data, $sql, $config);
		break;
	case "logoutmember":
		logout($data, $sql, $config);
		break;
	case "deleterobot":
		deleteRobot($data, $sql, $config);
		break;
	case "setrobotscore":
		updateScore($data, $sql, $config);
		break;
	case "setrwgames":
		resetBattleCounter($data, $sql, $config);
		break;
	default:
		echo "[#status:0]";
        break;
}

?>